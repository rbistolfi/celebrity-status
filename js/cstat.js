/* Celebrity Status */


var app = angular.module("cstat", ["cstatServices"]);


function CelebrityStatusController ($scope, $http, CelebrityStatus) {
    $scope.celebrities = [];
    $scope.add = function (name) {
	name = name.replace(/ /g, "_");
	var status = CelebrityStatus.get({name: name}, function() {
	    $scope.celebrities.push(status)
	    console.log(status);
	});
    }
}


// CelebrityStatusController.$inject = ["$scope", "CelebrityStatus"];
