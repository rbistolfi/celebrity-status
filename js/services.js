/* Celebrity Status Services */


var cstatServices = angular.module("cstatServices", ["ngResource"]);
var _backendURL = "http://celebritystatus-backend.herokuapp.com/:query";
var _backendURL = "http://0e067f21-toni.conrad.pre.3scale.net/:query"


cstatServices.factory("CelebrityStatus", ["$resource",
    function($resource) { 
	return $resource(
	    _backendURL,
	    {query: 'is_dead'}, 
	    {get: {method: "GET", headers: {"Accept": "application/json", "Access-Control-Request-Method": "GET"}}}
	);
    }]
);
